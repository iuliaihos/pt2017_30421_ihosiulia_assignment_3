package validators;

import database.ProductQuery;

import model.Product;

public class ProductValidator implements Validator<Product>{
	public void validate(Product t) {
		ProductQuery p = new ProductQuery();
		
		if(p.findById(t.getIdproduct())!= null)
			  throw new IllegalArgumentException("duplicate key");
		if (t.getName().length()>45)
			throw new IllegalArgumentException("max name field size is 45 characters");
}
}

package validators;

import java.util.regex.Pattern;

import database.ClientQuery;
import model.Client;


public class ClientValidator implements Validator<Client>{

	public void validate(Client t) {
		ClientQuery c = new ClientQuery();
		if(c.findById(t.getIdclient())!= null)
			  throw new IllegalArgumentException("duplicate key");
		if (t.getName().length()>45)
			throw new IllegalArgumentException("max name field size is 45 characters");
		if (t.getEmail().length()>45)
			throw new IllegalArgumentException("max email field size is 45 characters");
		if (t.getAddress().length()>100)
			throw new IllegalArgumentException("max address field size is 100 characters");
		if (Pattern.matches("^[0-9]*$",t.getPhone()) == false || t.getPhone().length()>10)
			throw new IllegalArgumentException("incorrect phone number format");
		
		
	    
	}
   
}

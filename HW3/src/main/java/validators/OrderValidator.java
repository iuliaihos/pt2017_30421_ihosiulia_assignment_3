package validators;

import database.ClientQuery;
import database.OrderQuery;
import database.ProductQuery;
import model.Order;

public class OrderValidator implements Validator<Order> {
	public void validate(Order t) {
		OrderQuery o = new OrderQuery();
		ProductQuery p = new ProductQuery();
		ClientQuery c = new ClientQuery();
		if(o.findById(t.getIdorder())!= null)
			  throw new IllegalArgumentException("duplicate key");
		if(c.findById(t.getClientid()) == null )
			  throw new IllegalArgumentException("the client id doesn't exist");
		if(p.findById(t.getProductid()) == null)
			  throw new IllegalArgumentException("the product id doesn't exist");
		if(p.findById(t.getProductid()).getStock()<t.getQuantity())
			  throw new IllegalArgumentException("insufficient stock");
		
	}
}

package businessLogic;

import java.util.ArrayList;
import java.util.List;
import database.ClientQuery;
import model.Client;

public class ClientAdministration {
	
	List<Client> clients;
	
	public ClientAdministration()
	{
		clients = new ArrayList<Client>();
		ClientQuery cq = new ClientQuery();
		clients = cq.findAll();
		
	}

	
	public String toString()
	{
		String s =" ";
		for(Client c :clients)
			s +=c.toString()+ "\n";
		return s;
	}
}

package businessLogic;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import database.ClientQuery;
import database.OrderQuery;
import database.ProductQuery;
import model.Client;
import model.Order;
import model.Product;

public class OrderProcessing {
	
		
		List<Order> orders;
		
		public OrderProcessing()
		{
			orders = new ArrayList<Order>();
			OrderQuery oq = new OrderQuery();
			orders = oq.findAll();
		}	
		
		public static void createBill(Order o)
		{
			
			ProductQuery pq = new ProductQuery();
			ClientQuery cq = new ClientQuery();
			Client c = cq.findById(o.getClientid());
			
		
			Product p = pq.findById(o.getProductid());
			
			
			try{
			    PrintWriter writer = new PrintWriter("bill-"+c.getName()+"-"+o.getIdorder()+".txt", "UTF-8");
			    writer.println("           Order no. "+ o.getIdorder());
			    writer.println(c);
			    writer.println("issued an order for:");
			    writer.println(p.getName()+ ". The quantity ordered is " + o.getQuantity());
			    writer.println("\n The final price is ");
			    writer.println(o.getQuantity()*p.getPrice());
			    writer.close();
			    
			} catch (IOException e) {
			   
			}
		}
		
		public String toString()
		{
			String s =" ";
			for(Order o : orders)
				s +=o.toString()+ "\n";
			return s;
		}
}

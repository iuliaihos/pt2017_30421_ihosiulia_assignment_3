package businessLogic;

import java.util.ArrayList;
import java.util.List;
import database.ProductQuery;
import model.Product;

public class WarehouseAdministration {
	
	List<Product> products;
	ProductQuery pq;
	
	public WarehouseAdministration()
	{
		products = new ArrayList<Product>();
		pq = new ProductQuery();
		products = pq.findAll();
	}	
	
	public  void decStock(int id, int c)
	{
		Product p = pq.findById(id);
		pq.updateStock(p.getStock() - c, id);
	}
	
	public  void incStock(int id, int c)
	{
		Product p = pq.findById(id);
		pq.updateStock(p.getStock() + c, id);
	}
	
	public String understockStats()
	{
		String s =" ";
		for(Product p : products)
			if(p.getStock() == 0)
		          s += p.getName();
		return s;
	}
	
	public String toString()
	{
		String s =" ";
		for(Product p : products)
			s +=p.toString()+ "\n";
		return s;
	}
}

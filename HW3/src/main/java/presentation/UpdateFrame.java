package presentation;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import businessLogic.OrderProcessing;
import businessLogic.WarehouseAdministration;
import database.ClientQuery;
import database.OrderQuery;
import database.ProductQuery;
import model.Client;
import model.Order;
import model.Product;
import validators.ClientValidator;
import validators.OrderValidator;
import validators.ProductValidator;

public class UpdateFrame {

	 
	  @SuppressWarnings("unused")
	private static final long serialVersionUID = 1L;
	  private JFrame frame;
	  private JPanel panel;
	  private JPanel panel2;
	  private JButton product;
	  private JButton client;
	  private JButton order;
	  private JButton p;
	  private JButton c;
	  private JButton o;
	  private JTextField toBeAdded;
	  
	 
	  
	  public UpdateFrame()
	  {
		  frame = new JFrame("Warehouse Management");
		  panel = new JPanel();
		  panel2 = new JPanel();
		  frame.setSize(700,400);
		  frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		  frame.setLayout(new GridLayout(4,4));
		  
		 
		  toBeAdded = new JTextField();
		  toBeAdded.setPreferredSize(new Dimension(200,30));
		  toBeAdded.setFont(new Font("SansSerif", Font.BOLD, 20));
		  
		 
		  product = new JButton("Add a new product");
		  client = new JButton("Add a new client");
		  order = new JButton("Add a new order");
		  

		  p = new JButton("Delete a product by id");
		  c = new JButton("Delete a client by id");
		  o = new JButton("Delete an order by id");
		  
		  panel.add(product);
		  panel.add(client);
		  panel.add(order);
		  panel.add(toBeAdded);
		  frame.add(panel);
		  panel2.add(p);
		  panel2.add(c);
		  panel2.add(o);
		  
		  frame.add(panel2);
		  frame.setVisible(true);
		  
		  product.addActionListener(
				  new ActionListener()
				  {
					  public void actionPerformed(ActionEvent e)
					  {
						 
						 ProductQuery q = new ProductQuery();
						 
						 String delim = "[ ]+";
						  String[] s = toBeAdded.getText().split(delim);
						  int id = Integer.parseInt(s[0]);
						  String name = s[1];
						  int stock = Integer.parseInt(s[2]);
						  double price = Double.parseDouble(s[3]);
						  Product p =new Product(id, name, stock, price);
						  ProductValidator v = new ProductValidator();
						  try{
							  v.validate(p);
						  }
						  catch(Exception e1)
						  {
							 JOptionPane.showMessageDialog(null,e1.getMessage());
						  }
						  q.insert(p);
						
					  }
				  }
				  );
		  p.addActionListener(
				  new ActionListener()
				  {
					  public void actionPerformed(ActionEvent e)
					  {
						 ProductQuery q = new ProductQuery();
						  int id = Integer.parseInt(toBeAdded.getText());
						  q.delById(id);
						
					  }
				  }
				  );
		  c.addActionListener(
				  new ActionListener()
				  {
					  public void actionPerformed(ActionEvent e)
					  {
						 
						  ClientQuery c = new ClientQuery();
						  int id = Integer.parseInt(toBeAdded.getText());
						  c.delById(id);
					  }
				  }
				  );
		  o.addActionListener(
				  new ActionListener()
				  {
					  public void actionPerformed(ActionEvent e)
					  {
						  OrderQuery oq = new OrderQuery();
						  int id = Integer.parseInt(toBeAdded.getText());
						  oq.delById(id);
					  }
				  }
				  );
		  
		  order.addActionListener(
				  new ActionListener()
				  {
					  public void actionPerformed(ActionEvent e)
					  {
						  OrderQuery oq = new OrderQuery();
							 
						  String delim = "[ ]+";
						  String[] s = toBeAdded.getText().split(delim);
						  int o = Integer.parseInt(s[0]);
						  int c = Integer.parseInt(s[1]);
						  int p = Integer.parseInt(s[2]);
						  int q = Integer.parseInt(s[3]);
						  Order or = new Order(o,c,p,q);
						  OrderValidator v = new OrderValidator();
						  try{
							  v.validate(or);
						  }
						  catch(Exception e1)
						  {
							 JOptionPane.showMessageDialog(null,e1.getMessage());
						  }
						  WarehouseAdministration wa =new   WarehouseAdministration();
						  wa.decStock(p, q);
						  oq.insert(or);
						  OrderProcessing.createBill(or);
					  }
				  }
				  );
		  
		 client.addActionListener(
				  new ActionListener()
				  {
					  public void actionPerformed(ActionEvent e)
					  {
						  ClientQuery c = new ClientQuery();
						  String delim = "/+";
						  String[] s = toBeAdded.getText().split(delim);
						  int id = Integer.parseInt(s[0]);
						  String name = s[1];
						  String address = s[2];
						  String email = s[3];
						  String phone = s[4];
						  Client client = new Client(id,  name, address, email,phone);
						  ClientValidator v = new  ClientValidator();
						  try{
							  v.validate(client);
						  }
						  catch(Exception e1)
						  {
							 JOptionPane.showMessageDialog(null,e1.getMessage());
						  }
						  c.insert(client);
					  }
				  }
				  );
	  }
	  
	}

package presentation;


import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import database.ClientQuery;
import database.OrderQuery;
import database.ProductQuery;

import model.Client;
import model.Order;
import model.Product;

public class Frame {
	
	 @SuppressWarnings("unused")
	private static final long serialVersionUID = 1L;
	  private JFrame frame;
	  private JPanel panel;
	  private JPanel panel3;
	  private JScrollPane scrollPane;
	  private JButton viewAllProducts;
	  private JButton viewAllClients;
	  private JButton viewAllOrders;
	  private JButton update;
	  private JButton search;
	  private JTable table;
	 
	  
	 
	public Frame()
	  {
		  
		  frame = new JFrame("Warehouse Management");
		  panel = new JPanel();
		 
		  panel3 = new JPanel();
		  frame.setSize(700,400);
		  frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		  frame.setLayout(new GridLayout(4,4));
		   
		  table = new JTable();
		  scrollPane = new JScrollPane(table);
		  table.setFillsViewportHeight(true);
		  frame.add(scrollPane);
		  
		  viewAllProducts = new JButton("View the products");
		  viewAllClients = new JButton("View the clients");
		  viewAllOrders = new JButton("View all orders");
		  viewAllProducts.addActionListener(
				  new ActionListener()
				  {
					public void actionPerformed(ActionEvent e) {
						
						  ProductQuery p = new ProductQuery();
						  ArrayList<Product> obj = p.findAll();
						  ArrayList<Object> ob = new ArrayList<Object>();
						  for (Product el:obj)
						  {
							  ob.add((Object)el);
						  }
						 
						  table.setModel(createTable(ob));
						 
						  
					}
					  
				  }
				  );
		  viewAllClients.addActionListener(
				  new ActionListener()
				  {
					public void actionPerformed(ActionEvent e) {
						
						  ClientQuery p = new ClientQuery();
						  ArrayList<Client> obj = p.findAll();
						  ArrayList<Object> ob = new ArrayList<Object>();
						  for (Client el:obj)
						  {
							  ob.add((Object)el);
						  }
						  
						  table.setModel(createTable(ob));
						 
						  
					}
					  
				  }
				  );
		  viewAllOrders.addActionListener(
				  new ActionListener()
				  {
					public void actionPerformed(ActionEvent e) {
						
						  OrderQuery p = new OrderQuery();
						  ArrayList<Order> obj = new ArrayList<Order>();
						  obj = p.findAll();
						  ArrayList<Object> ob = new ArrayList<Object>();
						  for (Order el:obj)
						  {
							  ob.add((Object)el);
							 
						  }
						 
						  table.setModel(createTable(ob));
						  
					}
					  
				  }
				  );
		  panel3.add(viewAllProducts);
		  panel3.add(viewAllClients);
		  panel3.add(viewAllOrders);
		  
		  
		  update = new JButton("Update database");
		  search= new JButton("Search");
		  update.addActionListener(
				  new ActionListener()
				  {
					public void actionPerformed(ActionEvent e) {
						
						 @SuppressWarnings("unused")
						UpdateFrame s = new UpdateFrame();
						  
					}
					  
				  }
				  );
		  search.addActionListener(
				  new ActionListener()
				  {
					public void actionPerformed(ActionEvent e) {
						
						@SuppressWarnings("unused")
						SearchFrame s = new SearchFrame(); 
						  
					}
					  
				  }
				  );
		  panel.add(update);
		  panel.add(search);
		  
		 
		  
		 
		  frame.add(panel3);
		  frame.add(panel);
		  frame.setVisible(true);
	  }

	private DefaultTableModel createTable(ArrayList<Object> list) {
		
		DefaultTableModel model = new DefaultTableModel();
		int count=0;
        for(Field field : list.get(0).getClass().getDeclaredFields())
		{
           model.addColumn(field.getName());
           count++;
		}
        String[] row = new String [count];
        for(Object e : list )
        {   
        	count = 0;
        	 for(Field field : e.getClass().getDeclaredFields())
     		{
                try {
                	field.setAccessible(true);
                	System.out.println(field.get(e).toString());
					row[count++]= field.get(e).toString();
				} catch (IllegalArgumentException e1) {
					e1.printStackTrace();
				} catch (IllegalAccessException e1) {
					e1.printStackTrace();
				}
     		}
        	model.addRow(row);
	    }
       
        model.fireTableDataChanged();
        return model;
	}
}


package presentation;

import java.awt.BorderLayout;
import java.awt.Dimension;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import database.ClientQuery;

import database.ProductQuery;

public class SearchFrame extends JFrame{
	
  private static final long serialVersionUID = 1L;
  private JFrame frame;
  private JPanel panel;
  private JButton search1;
  private JButton search2;
  private JButton search3;
  private JTextField toBeSearched;

  public SearchFrame()
  {
	  frame = new JFrame("Warehouse Management");
	  panel = new JPanel();
	  frame.setSize(700,400);
	  frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	  frame.setLayout(new BorderLayout());
	  
	  search1= new JButton("Search clients by id");
	  search2= new JButton("Search clients by name");
	  search3= new JButton("Search products");
	  toBeSearched = new JTextField();
	  toBeSearched.setPreferredSize(new Dimension(200,30));
	  toBeSearched.setFont(new Font("SansSerif", Font.BOLD, 20));
	  
	 
	 
	  
	  panel.add(search1);
	  panel.add(search2);
	  panel.add(search3);
	  panel.add(toBeSearched);
	  frame.add(panel);
	  frame.setVisible(true);
	  
	  search1.addActionListener(
			  new ActionListener()
			  {
				  public void actionPerformed(ActionEvent e)
				  {
					 int id = Integer.parseInt(toBeSearched.getText());
					 ClientQuery q = new ClientQuery();
					 if(q.findById(id) == null)
						 JOptionPane.showMessageDialog(null,"no such entry");
	                      else
					 JOptionPane.showMessageDialog(null,q.findById(id).toString());
				  }
			  }
			  );
	  search2.addActionListener(
			  new ActionListener()
			  {
				  public void actionPerformed(ActionEvent e)
				  {
					  String name = toBeSearched.getText();
					  ClientQuery q = new ClientQuery();
					  if(q.findByName(name) == null)
							 JOptionPane.showMessageDialog(null,"no such entry");
		                      else
						 JOptionPane.showMessageDialog(null,q.findByName(name).toString());
				  }
			  }
			  );
	  
	 search3.addActionListener(
			  new ActionListener()
			  {
				  public void actionPerformed(ActionEvent e)
				  {
					  ProductQuery q = new ProductQuery();
					  String name = toBeSearched.getText();
					  if(q.findByName(name) == null)
							 JOptionPane.showMessageDialog(null,"no such entry");
		                      else
						 JOptionPane.showMessageDialog(null,q.findByName(name).toString());
				  }
			  }
			  );
  }
  
}

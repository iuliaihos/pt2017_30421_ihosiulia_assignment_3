package model;

public class Product {
	private int idproduct;
	private String name;
	private int stock;
	private double price;
	
public Product(int i, String n, int s, double p)
{
	idproduct = i;
	name = n;
	stock = s;
	price = p;
}

public Product()
{
	
}
	public int getIdproduct() {
		return idproduct;
	}
	public void setIdproduct(int idproduct) {
		this.idproduct = idproduct;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getStock() {
		return stock;
	}
	public void setStock(int stock) {
		this.stock = stock;
	}
    
public void decStock(int c)
{
	stock -= c; 
}


	public String toString()
	{
		String s="";
		s += "Product with id: "+ idproduct + "; name: " + name + "; stock: " + stock;
		return s;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
}

package model;

public class Order {
	
	private int idorder;
    private int clientid;
	private int productid;
    private int quantity;
	  
	
  public Order(int o, int c,int p,int q)
  {
	  this.idorder = o;
	  this.clientid = c;
	  this.productid = p;
	  this.quantity = q;
  }
  
  public Order()
  {
	  
  }
  
  public String toString()
  {
	  String s="";
	  s += "Order no.: " + getIdorder() + " made by client no.: "+getClientid()+ " for product no.: " + getProductid();
	  s +="; the odered quantity is: " + getQuantity();
	  return s;
  }


public int getIdorder() {
	return idorder;
}


public void setIdorder(int idorder) {
	this.idorder = idorder;
}


public int getClientid() {
	return clientid;
}


public void setClientid(int clientid) {
	this.clientid = clientid;
}


public int getProductid() {
	return productid;
}


public void setProductid(int productid) {
	this.productid = productid;
}


public int getQuantity() {
	return quantity;
}


public void setQuantity(int quantity) {
	this.quantity = quantity;
}
}

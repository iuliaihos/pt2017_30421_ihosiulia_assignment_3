package model;

public class Client {
	private int idclient;
	private String name;
	private String address;
	private String email;
	private String phone;
	
public Client(int id, String name, String address, String email, String phone)
{
   this.setIdclient(id);
   this.setName(name);
   this.setAddress(address);
   this.setEmail(email);
   this.setPhone(phone);
}

public Client()
{
	
}
public String toString()
{
	String s = "";
	s+= "Customer " +getIdclient() + ": "+ getName() + "; Address: " + getAddress() + "; Email: " + getEmail() + "; Phone number: " + getPhone();
	return s;
}

public int getIdclient() {
	return idclient;
}

public void setIdclient(int id) {
	this.idclient = id;
}

public String getEmail() {
	return email;
}

public void setEmail(String email) {
	this.email = email;
}

public String getAddress() {
	return address;
}

public void setAddress(String address) {
	this.address = address;
}

public String getPhone() {
	return phone;
}

public void setPhone(String phone) {
	this.phone = phone;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}


}

package database;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AbstractQuery<T> {

  protected static final Logger LOGGER =Logger.getLogger(AbstractQuery.class.getName());
  private final Class<T> type;
  protected static ResultSet set;
  protected static PreparedStatement statement; 
  protected static Connection connection;
  
  @SuppressWarnings("unchecked")
public AbstractQuery()
  {
	 this.type = (Class<T>)((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	
  }
	 
  
  protected String createSelectQuery(String field)
  {
	  StringBuilder sb = new StringBuilder();
	  sb.append("SELECT * FROM warehouse.");
	  sb.append(type.getSimpleName().toLowerCase());
	  sb.append(" WHERE " + field + " =?");
	  return sb.toString();
  }
  
  private String createSelectAllQuery(String field)
  {
	  StringBuilder sb = new StringBuilder();
	  sb.append("SELECT * FROM warehouse.");
	  sb.append(type.getSimpleName().toLowerCase());
	  return sb.toString();
  }
  
  private String createDeleteQuery(String field)
  {
	  StringBuilder sb = new StringBuilder();
	  sb.append("Delete FROM warehouse.");
	  sb.append(type.getSimpleName().toLowerCase());
	  sb.append(" WHERE " + field + " =?");
	  return sb.toString();
  }
  
  private String createInsertQuery(String field)
  {
	  StringBuilder sb = new StringBuilder();
	  sb.append("INSERT INTO warehouse.");
	  sb.append(type.getSimpleName().toLowerCase());
	  sb.append(" values (");
	  for(@SuppressWarnings("unused") Field f: getType().getDeclaredFields())
		{
			sb.append("?, ");	
		}
	  sb.deleteCharAt(sb.length()-2);
	  sb.append(")");
	  return sb.toString();
  }
  
  public ArrayList<T> findAll()
  {
	  connection  = ConnectionFactory.getConnection();
	  String query = "";
	try {
		query = createSelectAllQuery(getType().getDeclaredFields()[0].getName());
	} catch (IllegalArgumentException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	} catch (SecurityException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	System.out.println(query);
	  try {
			statement =  connection.prepareStatement(query);
			set = statement.executeQuery();
			if(set.isBeforeFirst())  //returns false if result set is empty
			{
				return 	createObjects(set);
			}
			else
				System.out.println("inexistent entry");
		  } catch (SQLException e) {
			  LOGGER.log(Level.WARNING, getType().getName() + "AbstractQuery : findBYId " + e.getMessage());  
		         }
	        finally{
	        	close();
	        }
      return null;
  }
  
  public T findById(int id)
  {
	  T obj = null;
	  connection  = ConnectionFactory.getConnection();
	  String query = "";
	try {
		query = createSelectQuery(getType().getDeclaredFields()[0].getName());
	} catch (IllegalArgumentException e1) {
		e1.printStackTrace();
	} catch (SecurityException e1) {
		e1.printStackTrace();
	}
	System.out.println(query);
	  try {
			statement =  connection.prepareStatement(query);
			statement.setInt(1, id);
			set = statement.executeQuery();
			if(set.isBeforeFirst())  //returns false if result set is empty
			{
				obj = createObjects(set).get(0);
				return obj;
			}
		  } catch (SQLException e) {
			  LOGGER.log(Level.WARNING, getType().getName() + "AbstractQuery : findBYId " + e.getMessage());  
		         }
	        finally{
	        	close();
	        }
      return null;
  }
  
  private ArrayList<T> createObjects(ResultSet set) {
	List <T> list = new ArrayList<T>();
	
	
		try {
			while(true)
			{
				if(!set.next()) break;
				T instance =  getType().newInstance();;

					for(Field field : getType().getDeclaredFields())
					{
						
						Object value;
					    value = set.getObject(field.getName());
					    PropertyDescriptor pd;
					    pd = new PropertyDescriptor(field.getName() ,getType());
					    Method method = pd.getWriteMethod();
					    System.out.println(method.getName());
					    method.invoke(instance, value);
					}
					
					list.add(instance);
					
					
			}
				} catch (InstantiationException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
			    } catch (SQLException e) {
					e.printStackTrace();
				}catch (IntrospectionException e) {
				    e.printStackTrace();
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
				    e.printStackTrace();
				}catch (SecurityException e) {
		            e.printStackTrace();}
		
		return (ArrayList<T>) list;
}
  
  public void delById(int id)
  {
	 
	  connection  = ConnectionFactory.getConnection();
	  String query = "";
	try {
		query = createDeleteQuery(getType().getDeclaredFields()[0].getName());
	   
	} catch (IllegalArgumentException e) {
		e.printStackTrace();
	} catch (SecurityException e) {
		e.printStackTrace();
	}
	System.out.println(query);
	  try {
			statement =  connection.prepareStatement(query);
			statement.setInt(1, id);
			statement.execute();
		  } catch (SQLException e) {
			  LOGGER.log(Level.WARNING, getType().getName() + "AbstractQuery : findBYId " + e.getMessage());  
		         }
	        finally{
	        	close();
	        }
  }
  
  public void insert(T t) {
		
	  connection  = ConnectionFactory.getConnection();
	  String query = "";
	  
	  try {
		query = createInsertQuery(getType().getDeclaredFields()[0].getName());
		 System.out.println(query);
	} catch (IllegalArgumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (SecurityException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	  try {
		statement = connection.prepareStatement(query);
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	  int i=1;
	  for(Field field : getType().getDeclaredFields())
		{
			try {
				field.setAccessible(true);
				statement.setObject(i++, field.get(t));
				System.out.println(field.get(t));
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	  
      try {
		statement.execute();
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
			
			
			
	        

		}

protected static void close()
  {
	    ConnectionFactory.close(set);
		ConnectionFactory.close(statement);
		ConnectionFactory.close(connection);
  }

public Class<T> getType() {
	return type;
}
}

package database;

import java.sql.SQLException;


import model.Product;

public class ProductQuery extends AbstractQuery<Product>{
	
	private final static String update = "UPDATE product SET stock = ? WHERE idproduct =?";
	private final static String findByName = "SELECT * FROM product where product.name = ?";
	
	public ProductQuery()
	{ 
		super();
	}

	public void updateStock(int c,int b)
	  {
		  
		  connection = ConnectionFactory.getConnection();
	      
	      try {
			statement = connection.prepareStatement(update);
			statement.setInt(1,c);
			statement.setInt(2,b);
			statement.execute();
		
		} catch (SQLException e) {
		
		}
	      close();

	  }
    
	public Product findByName(String n )
	  {
		  connection = ConnectionFactory.getConnection();
		  Product c = null;
		 
		  try {
			statement = connection.prepareStatement(findByName);
			statement.setString(1, n);
			set = statement.executeQuery();
			if(set.isBeforeFirst())  //returns false if result set is empty
			{
			   set.next();
			   int idproduct = set.getInt("idproduct");
			   String name = set.getString("name");
			   int stock = set.getInt("stock");
			   double price = set.getDouble("price");
		       c = new Product(idproduct,name,stock,price);
		       close();
			   return c;
		  }
		  } catch (SQLException e) {
			         e.printStackTrace();
		         }
		  close();
	      return null;
	  }
}

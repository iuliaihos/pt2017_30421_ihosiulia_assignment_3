package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
/*
 * singleton class used for connecting to the database "warehouse"
 */
public class ConnectionFactory {
  private static final Logger LOGGER = Logger.getLogger(Connection.class.getName());
  private static final String DRIVER = "com.mysql.jdbc.cj.Driver";
  private static final String DBURL = "jdbc:mysql://localhost:3306/warehouse";
  private static final String USER = "root";
  private static final String PASS = "moscraciun";
  	
  private static ConnectionFactory singleInstance = new ConnectionFactory();
  
  private ConnectionFactory()
  {
	  try{
		  Class.forName(DRIVER);
	  }
	  catch(ClassNotFoundException e)
	  {
		  e.printStackTrace();
	  }
  }
  
 
  
  private Connection createConnection()
  {
	  try {
			Connection c = DriverManager.getConnection(DBURL,USER,PASS);
			return c;
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Couldn't connect to the database");
			e.printStackTrace();
		}
		return null;
	  
  }
  
  public static Connection getConnection()
  {
	
	return singleInstance.createConnection();
	  
  }
  
  public static void close(Connection c)
  {
	  try {
		c.close();
	} catch (SQLException e) {
		
		LOGGER.log(Level.WARNING, "An error occured while trying to close the connection");
	}
  }
  
  public static void close(Statement s)
  {
	  try {
		s.close();
	} catch (SQLException e) {
		LOGGER.log(Level.WARNING, "An error occured while trying to close the statement");
	}
  }
  
  public static void close(ResultSet r)
  {
	  try {
		r.close();
	} catch (SQLException e) {
		LOGGER.log(Level.WARNING, "An error occured while trying to close the result set");
	}
  }
}

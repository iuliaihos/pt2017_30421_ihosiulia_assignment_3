package database;

import java.sql.SQLException;

import model.Client;

public class ClientQuery  extends AbstractQuery<Client>{
	
	 private final static String findCustomerByName = "SELECT * FROM client where name = ?";
	public ClientQuery()
	{
		super();
	}

	public Client findByName(String n )
	  {
		  connection = ConnectionFactory.getConnection();
		  Client c = null;
		 
		  try {
			statement = connection.prepareStatement(findCustomerByName);
			statement.setString(1, n);
			set = statement.executeQuery();
			
			if(set.isBeforeFirst())  //returns false if result set is empty
			{
			set.next();
			int idclient = set.getInt("idclient");
			String name = set.getString("name");
		    String address = set.getString("address");
			String email = set.getString("email");
			String phone = set.getString("phone");
			
			c = new Client(idclient,name,address,email,phone);
			
			close();
			return c;
			}
		  } catch (SQLException e) {
			         e.printStackTrace();
		         }
		  close();
	      return null;
	  }
}
